from bson.objectid import ObjectId
# import motor.motor_asyncio
from datetime import datetime
from pymongo.mongo_client import MongoClient
from pymongo.server_api import ServerApi

MONGO_DETAILS = f"mongodb+srv://priest:Grutika58@cluster0.p3votzq.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0"

# client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)
client = MongoClient(MONGO_DETAILS, server_api=ServerApi('1'))

database = client.fAPI
if database:
    print("INFO::::: DB connected.")
else:
    print("error::::: DB not connected.")

admin_collection = database.get_collection("admins")


# helpers
def admn_helper(admin) -> dict:
    return {
        "id": str(admin["_id"]),
        "fullname": admin["fullname"],
        "email": admin["email"],
        "phone": admin["phone"],
        "date_comparer": datetime.now().timestamp(),
    }

def admin_helper(admin) -> dict:
    return {
        "id": str(admin["_id"]),
        "fullname": admin["fullname"],
        "email": admin["email"],
        "phone": admin["phone"],
        "date_comparer": admin["date_comparer"],
    }

# Retrieve all admins present in the database
async def retrieve_admins():
    admins = []
    for admin in list(admin_collection.find()):
        admins.append(admin_helper(admin))
    return admins


# Add a new admin into to the database
async def add_admin(admin_data: dict) -> dict:
    admin = admin_collection.insert_one(admin_data)
    new_admin = admin_collection.find_one({"_id": admin.inserted_id})
    return admn_helper((new_admin))


# Retrieve a admin with a matching ID
async def retrieve_admin(id: str) -> dict:
    admin = admin_collection.find_one({"_id": ObjectId(id)})
    if admin:
        return admin_helper(admin)


# Update a admin with a matching ID
async def update_admin(id: str, data: dict):
    # Return false if an empty request body is sent.
    if len(data) < 1:
        return False
    admin = admin_collection.find_one({"_id": ObjectId(id)})
    if admin:
        data['date_comparer'] = datetime.now().timestamp()
        updated_admin = admin_collection.update_one(
            {"_id": ObjectId(id)}, {"$set": data}
        )
        if updated_admin:
            return True
        return False


# Delete a admin from the database
async def delete_admin(id: str):
    admin = admin_collection.find_one({"_id": ObjectId(id)})
    if admin:
        admin_collection.delete_one({"_id": ObjectId(id)})
        return True

