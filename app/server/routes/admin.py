from fastapi import APIRouter, Body
from fastapi.encoders import jsonable_encoder

from server.database import (
    add_admin,
    delete_admin,
    retrieve_admin,
    retrieve_admins,
    update_admin,
)
from server.models.admin import (
    ErrorResponseModel,
    ResponseModel,
    AdminSchema,
    UpdateAdminModel,
)

router = APIRouter()

@router.post("/", response_description="Admin data added into the database")
async def add_admin_data(admin: AdminSchema = Body(...)):
    admin = jsonable_encoder(admin)
    new_admin = await add_admin(admin)
    return ResponseModel(new_admin, "Admin added successfully.")

# {
#     "fullname": "John Doe",
#     "email": "jdoe@x.edu.ng",
#     "course_of_study": "Water resources engineering",
#     "year": 2,
#     "gpa": "3.0",
# }

@router.get("/", response_description="Admins retrieved")
async def get_admins():
    admins = await retrieve_admins()
    if admins:
        return ResponseModel(admins, "Admins data retrieved successfully")
    return ResponseModel(admins, "Empty list returned")


@router.get("/{id}", response_description="Admin data retrieved")
async def get_admin_data(id):
    admin = await retrieve_admin(id)
    if admin:
        return ResponseModel(admin, "Admin data retrieved successfully")
    return ErrorResponseModel("An error occurred.", 404, "Admin doesn't exist.")

@router.put("/{id}")
async def update_admin_data(id: str, req: UpdateAdminModel = Body(...)):
    req = {k: v for k, v in req.dict().items() if v is not None}
    updated_admin = await update_admin(id, req)
    if updated_admin:
        return ResponseModel(
            "Admin with ID: {} name update is successful".format(id),
            "Admin name updated successfully",
        )
    return ErrorResponseModel(
        "An error occurred",
        404,
        "There was an error updating the admin data.",
    )


@router.delete("/{id}", response_description="Admin data deleted from the database")
async def delete_admin_data(id: str):
    deleted_admin = await delete_admin(id)
    if deleted_admin:
        return ResponseModel(
            "Admin with ID: {} removed".format(id), "Admin deleted successfully"
        )
    return ErrorResponseModel(
        "An error occurred", 404, "Admin with id {0} doesn't exist".format(id)
    )

