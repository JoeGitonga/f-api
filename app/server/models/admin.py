from typing import Optional

from pydantic import BaseModel, EmailStr, Field
from datetime import datetime


class AdminSchema(BaseModel):
    fullname: str = Field(...) # ... required field. can be replaced with 'None' or default value
    email: EmailStr = Field(...)
    phone: int = Field(...)
    date_comparer: str = Field(datetime.now().timestamp())

    # year: int = Field(..., gt=0, lt=9) #gt and lt stands for greater than= and less than=
    # gpa: float = Field(..., le=4.0) # le stands for less than or equal to

    class Config:
        schema_extra = {
            "example": {
                "fullname": "John Doe",
                "email": "jdoe@x.edu.ng",
                "phone": 254798657432,
                "date_comparer": 63498648.5784798,
            }
        }


class UpdateAdminModel(BaseModel):
    fullname: Optional[str]
    email: Optional[EmailStr]
    phone: Optional[int]
    date_comparer: str = Field(datetime.now().timestamp())
    class Config:
        schema_extra = {
            "example": {
                "fullname": "John Doe",
                "email": "jdoe@x.edu.ng",
                "phone": 254798657432,
                "date_comparer": 63498648.5784798,
            }
        }


def ResponseModel(data, message):
    return {
        "data": [data],
        "code": 200,
        "message": message,
    }


def ErrorResponseModel(error, code, message):
    return {"error": error, "code": code, "message": message}
