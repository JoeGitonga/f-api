from fastapi import FastAPI

from server.routes.admin import router as AdminRouter

app = FastAPI()

app.include_router(AdminRouter, tags=["Admin"], prefix="/admin")


@app.get("/", tags=["Root"])
async def read_root():
    return {"message": "Welcome to this awesome app!"}
